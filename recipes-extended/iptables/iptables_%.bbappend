
FILESEXTRAPATHS_prepend:="${THISDIR}/${PN}:"
SRC_URI += "file://iptables.service \
            file://iptables.rules \
"
inherit systemd

do_install_append() {
  install -d ${D}${sysconfdir}/iptables
  install -m 0644 ${WORKDIR}/iptables.rules ${D}${sysconfdir}/iptables

  install -d ${D}${systemd_system_unitdir}
  install -m 0644 ${WORKDIR}/iptables.service ${D}${systemd_system_unitdir}
}

SYSTEMD_SERVICE_${PN} = "iptables.service"
