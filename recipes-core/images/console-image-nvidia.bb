SUMMARY = "A console development image with some C/C++ dev tools"

IMAGE_FEATURES += "package-management splash"
IMAGE_LINGUAS = "en-us"

inherit image

DEPENDS += "deviceos-users"

CORE_OS = " \
    kernel-modules \
    openssh openssh-keygen openssh-sftp-server \
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \    
    packagegroup-core-buildessential \
    term-prompt \
    tzdata \
"

WIFI_SUPPORT = " \
    crda \
    iw \
    wpa-supplicant \
"

BLUETOOTH_SUPPORT = " \
    bluez5 \
"

DEV_SDK_INSTALL = " \
    binutils \
    binutils-symlinks \
    coreutils \
    cpp \
    cpp-symlinks \
    diffutils \
    elfutils elfutils-binutils \
    file \
    g++ \
    g++-symlinks \
    gcc \
    gcc-symlinks \
    gdb \
    gdbserver \
    gettext \
    git \
    ldd \
    libstdc++ \
    libstdc++-dev \
    libtool \
    ltrace \
    make \
    nodejs \
    pkgconfig \
    python3-modules \
    strace \
    openssl-dev \
    zlib-dev \
"

DEV_EXTRAS = " \
"

EXTRA_TOOLS_INSTALL = " \
    bzip2 \
    devmem2 \
    dosfstools \
    ethtool \
    fbset \
    findutils \
    grep \
    i2c-tools \
    iperf3 \
    iproute2 \
    iptables \
    less \
    nano \
    netcat-openbsd \
    nmap \
    ntp ntp-tickadj \
    procps \
    rng-tools \
    sysfsutils \
    unzip \
    util-linux \
    wget \
    zip \
    jq \
"

EXTRA_WW = " \
cmake \
curl \
daemontools \
dhcp-client \
e2fsprogs \
bash-completion \
git-perltools \
glibc-gconvs \
glibc-utils \
gnutls-openssl \
hostapd \
go \
iputils-ping \
jansson \
jansson-dev \
kernel-dev \
libevent \
libevent-dev \
liblockfile \
libmbim \
libnss-mdns \
libuv \
lsof \
ltp \
modemmanager \
minicom \
ncurses-dev \
perl \
parted \
ppp \
python-pip \
python3 \
readline \
rsync \
screen \
setserial \
socat \
sysstat \
tmux \
update-rc.d \
usb-modeswitch \
usbutils \
util-linux-agetty \
util-linux-bash-completion \
util-linux-uuidd \
valgrind \
wget-locale-zh-cn \
wget-locale-zh-tw \
xz \
"

PELION_STUFF = " \
    mbed-devicejs-bridge \
    mbed-fcc \
    node-hotplug \
"

WIGWAG_STUFF = " \
    devicejs \
    deviceoswd \
    emacs \
    fftw \
    imagemagick \
    lcms \
    node-hotplug \
    panic \
    pps-tools \
    pwgen \
    twlib \
    devicedb \
    maestro \
    deviceos-users \
    global-node-modules \
    wwrelay-utils \
    relay-term \
    mbed-edge-examples \
"

OPENSSL_102 = " \
    openssl \
"

DEV_IMAGE_INSTALL = " \
    ${DEV_SDK_INSTALL} \
    ${DEV_EXTRAS} \
    ${EXTRA_TOOLS_INSTALL} \
    ${EXTRA_WW} \
"

IMAGE_INSTALL += " \
    ${CORE_OS} \
    ${WIFI_SUPPORT} \
    ${BLUETOOTH_SUPPORT} \
    ${PELION_STUFF} \
    ${OPENSSL_102} \
    ${MACHINE_EXTRA_RRECOMMENDS} \
"

set_local_timezone() {
    ln -sf /usr/share/zoneinfo/EST5EDT ${IMAGE_ROOTFS}/etc/localtime
}

disable_bootlogd() {
    echo BOOTLOGD_ENABLE=no > ${IMAGE_ROOTFS}/etc/default/bootlogd
}

ROOTFS_POSTPROCESS_COMMAND += " \
    set_local_timezone ; \
    disable_bootlogd ; \
"

export IMAGE_BASENAME = "console-image"

#do_image_wic[depends] += "virtual/atf:do_deploy"

# Convince the task that creates image_license.manifest to include atf.
#do_populate_lic_deploy[depends] += "virtual/atf:do_deploy"
