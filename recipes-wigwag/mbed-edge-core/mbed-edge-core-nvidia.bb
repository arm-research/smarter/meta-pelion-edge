require mbed-edge-core.inc

COMPATIBLE_MACHINE = "(tegra)"

PROVIDES += " virtual/mbed-edge-core virtual/mbed-edge-core-dbg "
RPROVIDES_${PN} += " virtual/mbed-edge-core virtual/mbed-edge-core-dbg "

FILESEXTRAPATHS_prepend := "${THISDIR}/files/nvidia:"
SRC_URI += "file://target.cmake \
            file://sotp_fs_nvidia_yocto.h \
            file://arm_update_cmdline.sh \
            file://arm_update_activate.sh \
            file://arm_update_active_details.sh \
            file://arm_update_prepare.sh \
            file://0001-change-path-to-upgrade-scripts.patch \
            file://0002-fix_thread_priority.patch \
            "

do_install_append() {
    install -m 755 "${WORKDIR}/arm_update_cmdline.sh"        "${D}/wigwag/mbed"
    install -m 755 "${WORKDIR}/arm_update_activate.sh"       "${D}/wigwag/mbed"
    install -m 755 "${WORKDIR}/arm_update_active_details.sh" "${D}/wigwag/mbed"
    install -m 755 "${WORKDIR}/arm_update_prepare.sh"        "${D}/wigwag/mbed"
    install -d "${D}/wigwag/log"
    install -d "${D}/userdata/"
}

pkg_postinst_ontarget_${PN}() {
   ln -sf /dev/random /dev/hwrng
}
