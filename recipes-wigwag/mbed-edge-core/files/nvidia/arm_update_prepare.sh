#!/bin/sh
# ----------------------------------------------------------------------------
set -e
set -x
# Parse command line
#
# HEADER
# FIRMWARE
# LOCATION
# OFFSET
# SIZE
#
. /wigwag/mbed/arm_update_cmdline.sh

echo "-------------------- Executing prepare.sh -------------------------"
echo Stopping services
systemctl stop kubelet || true
systemctl stop docker || true
MEM=`free -m  | grep Mem | awk '{print $2}'`
echo Free memory $MEM

echo Creating tmpfs
mkdir -p /mnt/upgrades
mount -t tmpfs -o size=8g tmpfs /mnt/upgrades
echo "-------------------- Finished prepare.sh -------------------------"
exit $VALUE

