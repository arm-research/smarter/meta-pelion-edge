#!/bin/sh
# ----------------------------------------------------------------------------
# Parse command line
#
# HEADER
# FIRMWARE
# LOCATION
# OFFSET
# SIZE
#
. /wigwag/mbed/arm_update_cmdline.sh

echo "-------------------- Executing active_details.sh -------------------------"
# copy stored header to expected location
# Freshly flashed devices do not have the file there.
# This file is created after the first upgrade
cp /userdata/extended/header.bin $HEADER || true

AMOUNT_SLOTS=`nvbootctrl get-number-slots`
echo Boot slots found $AMOUNT_SLOTS
if [ "${AMOUNT_SLOTS}" != "2" ]; then
    echo A/B was not enabled
    nv_update_engine --enable-ab
fi

CURRENT_SLOT=`nvbootctrl get-current-slot`

if [ ${CURRENT_SLOT} = "0" ]; then 
    FLASH_SLOT="1"
elif [ ${CURRENT_SLOT} = "1" ]; then
    FLASH_SLOT="0"
else
    echo Error CURRENT_SLOT=${CURRENT_SLOT} cannot be parsed
    exit 1 
fi
FLASH_SUFFIX=`nvbootctrl get-suffix ${FLASH_SLOT}`
FLASH_PARTITION=`blkid -l -t PARTLABEL=APP${FLASH_SUFFIX} | cut -d: -f1`
echo FLASH_PART=\"${FLASH_PARTITION}\" > /mnt/upgrades/ota_target_partition
echo FLASH_SLOT=\"${FLASH_SLOT}\" >> /mnt/upgrades/ota_target_partition

echo "-------------------- Finished active_details.sh -------------------------"
