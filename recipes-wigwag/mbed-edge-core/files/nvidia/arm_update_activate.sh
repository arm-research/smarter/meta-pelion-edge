#!/bin/sh
# ----------------------------------------------------------------------------

# Parse command line
#
# HEADER
# FIRMWARE
# LOCATION
# OFFSET
# SIZE
#
. /wigwag/mbed/arm_update_cmdline.sh

echo "-------------------- Executing activate.sh -------------------------"
mkdir -p /userdata/extended
# copy header to store
VALUE=$(cp $HEADER /userdata/extended/header.bin)

mkdir -p /userdata/.logs-before-upgrade
cp -R /var/log/* /userdata/.logs-before-upgrade/

OTA_DIR=/mnt/upgrades
. /mnt/upgrades/ota_target_partition
unzip $FIRMWARE -d ${OTA_DIR}
. ${OTA_DIR}/flashvars 

simg2img ${OTA_DIR}/${IMAGE} ${OTA_DIR}/${IMAGE}.raw
dd if=${OTA_DIR}/${IMAGE}.raw of=${FLASH_PART} bs=1M

#the BUP will change the slot
#nvbootctrl set-active-boot-slot ${FLASH_SLOT}

mkdir -p /opt/ota_package
mv ${OTA_DIR}/bl_update_payload /opt/ota_package/bl_update_payload
nv_update_engine --install no-reboot 

cp $HEADER /userdata/extended/header.bin

# TODO: Enable watchdog
reboot
echo "-------------------- Finished activate.sh -------------------------"
exit $VALUE

